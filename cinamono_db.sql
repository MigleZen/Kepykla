-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 2018 m. Geg 08 d. 22:17
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.0.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cinamono_db`
--

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `atsiliepimai`
--

CREATE TABLE `atsiliepimai` (
  `atsiliepimoID` int(11) NOT NULL,
  `avardas` varchar(20) COLLATE utf8_lithuanian_ci NOT NULL,
  `atsiliepimas` text COLLATE utf8_lithuanian_ci NOT NULL,
  `data` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

--
-- Sukurta duomenų kopija lentelei `atsiliepimai`
--

INSERT INTO `atsiliepimai` (`atsiliepimoID`, `avardas`, `atsiliepimas`, `data`) VALUES
(1, 'Miglė', 'Jūsų gaminiai tiesiog tirpsta burnoje!', '2018-04-17'),
(5, 'Kitas', 'Labai skanu ir pan.', '2018-04-17');

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `naudotojas`
--

CREATE TABLE `naudotojas` (
  `naudotojoID` int(11) NOT NULL,
  `prisijungimoVardas` varchar(50) COLLATE utf8_lithuanian_ci NOT NULL,
  `vardas` varchar(50) COLLATE utf8_lithuanian_ci NOT NULL,
  `pavarde` varchar(50) COLLATE utf8_lithuanian_ci NOT NULL,
  `pastas` varchar(50) COLLATE utf8_lithuanian_ci NOT NULL,
  `numeris` varchar(50) COLLATE utf8_lithuanian_ci NOT NULL,
  `slaptazodis` varchar(50) COLLATE utf8_lithuanian_ci NOT NULL,
  `data` date NOT NULL,
  `tipas` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci ROW_FORMAT=COMPACT;

--
-- Sukurta duomenų kopija lentelei `naudotojas`
--

INSERT INTO `naudotojas` (`naudotojoID`, `prisijungimoVardas`, `vardas`, `pavarde`, `pastas`, `numeris`, `slaptazodis`, `data`, `tipas`) VALUES
(1, 'admin', 'Administratorius', 'Adminas', 'admin@admin.com', '866666666', 'd8578edf8458ce06fbc5bb76a58c5ca4', '2018-03-07', 1),
(2, 'Jonas', 'Jonas', 'Jonaitis', 'jonaitis@gmail.com', '863333333', '42a92d3b636db32fef4edb3308e0804d', '2018-03-28', 0);

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `patvirtinta`
--

CREATE TABLE `patvirtinta` (
  `uzsakymoNr` varchar(100) COLLATE utf8_lithuanian_ci NOT NULL,
  `naudotojoID` int(11) NOT NULL,
  `prekiuKiekis` int(11) NOT NULL,
  `uzsakymoKaina` float NOT NULL,
  `data` date NOT NULL,
  `busena` varchar(50) COLLATE utf8_lithuanian_ci DEFAULT 'Užsakyta'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

--
-- Sukurta duomenų kopija lentelei `patvirtinta`
--

INSERT INTO `patvirtinta` (`uzsakymoNr`, `naudotojoID`, `prekiuKiekis`, `uzsakymoKaina`, `data`, `busena`) VALUES
('2-2018-04-29', 2, 4, 3.84, '2018-05-08', 'Pagaminta'),
('2-2018-05-08', 2, 4, 6.1, '2018-05-08', 'Užsakyta');

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `prekes`
--

CREATE TABLE `prekes` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_lithuanian_ci NOT NULL,
  `image` varchar(200) COLLATE utf8_lithuanian_ci NOT NULL,
  `text` text COLLATE utf8_lithuanian_ci NOT NULL,
  `price` float NOT NULL,
  `type` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci ROW_FORMAT=COMPACT;

--
-- Sukurta duomenų kopija lentelei `prekes`
--

INSERT INTO `prekes` (`id`, `name`, `image`, `text`, `price`, `type`) VALUES
(6, '\"Obuoliukas\"', 'bandele4.jpg', 'MIELINĖS TEŠLOS GAMINYS SU ŠVIEŽIAIS OBUOLIAIS.', 0.5, 0),
(14, 'Keksiukas su oreo', 'keksiukas4.png', 'KEKSIUKAS SU OREO SAUSAINIŲ SVIESTINIU KREMU.', 1.42, 1),
(15, 'Vanilinis keksiukas', 'keksiukas2.png', 'VANILINIS KEKSIUKAS SU CITRINŲ SKONIO GLAISTU.', 1.56, 1),
(17, 'Karamelinė bandelė', 'bandele3.jpg', 'SLUOKSNIUOTOS MIELINĖS TEŠLOS GAMINYS SU KARAMELINIU ĮDARU.', 0.6, 0),
(18, 'Keksiukas su belgišku šokoladu', 'keksiukas3.png', 'ŠOKOLADINIS KEKSIUKAS SU BELGIŠKO ŠOKOLADO GLAISTU.', 1.53, 1),
(19, 'Bandelė \"Vėjo rožė\"', 'bandele1.jpg', 'SLUOKSNIUOTOS MIELINĖS TEŠLOS GAMINYS SU PLIKYTU KREMU.', 0.5, 0);

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `uzsakymai`
--

CREATE TABLE `uzsakymai` (
  `uzsakymoNr` varchar(50) COLLATE utf8_lithuanian_ci NOT NULL,
  `uzs_id` int(11) NOT NULL,
  `prekes_id` int(11) NOT NULL,
  `naudotojoID` int(11) NOT NULL,
  `prekes_pav` varchar(50) COLLATE utf8_lithuanian_ci NOT NULL,
  `vnt` int(30) NOT NULL,
  `data` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

--
-- Sukurta duomenų kopija lentelei `uzsakymai`
--

INSERT INTO `uzsakymai` (`uzsakymoNr`, `uzs_id`, `prekes_id`, `naudotojoID`, `prekes_pav`, `vnt`, `data`) VALUES
('2-2018-04-29', 9, 6, 2, '\"Obuoliukas\"', 2, '2018-04-29'),
('2-2018-04-29', 10, 14, 2, 'Keksiukas su oreo', 2, '2018-04-29'),
('2-2018-05-08', 11, 15, 2, 'Vanilinis keksiukas', 3, '2018-05-08'),
('2-2018-05-08', 12, 14, 2, 'Keksiukas su oreo', 1, '2018-05-08');

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `zinute`
--

CREATE TABLE `zinute` (
  `zinutes_id` int(11) NOT NULL,
  `kvardas` varchar(50) COLLATE utf8_lithuanian_ci NOT NULL,
  `kpastas` varchar(50) COLLATE utf8_lithuanian_ci NOT NULL,
  `zinute` text COLLATE utf8_lithuanian_ci NOT NULL,
  `data` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

--
-- Sukurta duomenų kopija lentelei `zinute`
--

INSERT INTO `zinute` (`zinutes_id`, `kvardas`, `kpastas`, `zinute`, `data`) VALUES
(2, 'Vardenis', 'pavardenis@gmail.com', 'Puikūs kepiniai!', '2018-03-30'),
(3, 'Vardas', 'varpav@gmail.com', 'Labai skanu!', '2018-03-30');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `atsiliepimai`
--
ALTER TABLE `atsiliepimai`
  ADD PRIMARY KEY (`atsiliepimoID`);

--
-- Indexes for table `naudotojas`
--
ALTER TABLE `naudotojas`
  ADD PRIMARY KEY (`naudotojoID`);

--
-- Indexes for table `patvirtinta`
--
ALTER TABLE `patvirtinta`
  ADD PRIMARY KEY (`uzsakymoNr`);

--
-- Indexes for table `prekes`
--
ALTER TABLE `prekes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `uzsakymai`
--
ALTER TABLE `uzsakymai`
  ADD PRIMARY KEY (`uzs_id`);

--
-- Indexes for table `zinute`
--
ALTER TABLE `zinute`
  ADD PRIMARY KEY (`zinutes_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `atsiliepimai`
--
ALTER TABLE `atsiliepimai`
  MODIFY `atsiliepimoID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `naudotojas`
--
ALTER TABLE `naudotojas`
  MODIFY `naudotojoID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `prekes`
--
ALTER TABLE `prekes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `uzsakymai`
--
ALTER TABLE `uzsakymai`
  MODIFY `uzs_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `zinute`
--
ALTER TABLE `zinute`
  MODIFY `zinutes_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
