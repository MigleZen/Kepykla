$(function(){
    $('#prisijungimoVardas_error').hide();
    $('#vardas_error').hide();
    $('#pavarde_error').hide();
    $('#pastas_error').hide();
    $('#numeris_error').hide();
    $('#slaptazodis_error').hide();
    $('#slaptazodis2_error').hide();
    
    
    var error_pvardas = false;
    var error_vardas = false;
    var error_pavarde = false;
    var error_pastas = false;
    var error_numeris = false;
    var error_slaptazodis = false;
    var error_slaptazodis2 = false;
 
    
    
    $('#prisijungimoVardas').focusout(function(){
        tikrink_prisijungimoVardas();
    });
    
    $('#vardas').focusout(function(){
        tikrink_vardas();
    });
    
    $('#pavarde').focusout(function(){
        tikrink_pavarde();
    });
    
    $('#pastas').focusout(function(){
        tikrink_pastas();
    });
    
    $('#numeris').focusout(function(){
        tikrink_numeris();
    });
    
    $('#slaptazodis').focusout(function(){
        tikrink_slaptazodis();
    });
    
    $('#slaptazodis2').focusout(function(){
        tikrink_slaptazodis2();
    });
    
   
    
    
    function tikrink_prisijungimoVardas(){
           
        var pvardas_ilgis = $("#prisijungimoVardas").val().length;
        
        if(pvardas_ilgis < 5 || pvardas_ilgis > 20){
            $('#prisijungimoVardas_error').html("Prisijungimo vardas turėtų būti 5 - 20 simbolių ilgio");
            $('#prisijungimoVardas_error').show();
            error_pvardas = true;
        } else {
            $('#prisijungimoVardas_error').hide();
        }
    }
    
    
    function tikrink_vardas(){
       
        var vardas_ilgis = $("#vardas").val().length;
        
        if(vardas_ilgis < 1){
            $('#vardas_error').html("Nepalikite tuščio lauko");
            $('#vardas_error').show();
            error_vardas = true;
        } else {
            $('#vardas_error').hide();
        }
    }
    
    function tikrink_pavarde(){
       
        var pavarde_ilgis = $("#pavarde").val().length;
        
        if(pavarde_ilgis < 1){
            $('#pavarde_error').html("Nepalikite tuščio lauko");
            $('#pavarde_error').show();
            error_pavarde = true;
        } else {
            $('#pavarde_error').hide();
        }
    }
    
     function tikrink_pastas(){
       
        var sablonas = new RegExp(/^[+a-zA-Z0-9._~]+@[a-zA-Z0-9.~]+\.[a-zA-Z]{2,4}$/i);
        
        if(sablonas.test($('#pastas').val())){
            $('#pastas_error').hide();
            
        } else {
            
            $('#pastas_error').html("Blogas el. paštas");
            $('#pastas_error').show();
            error_pastas = true;
        }
    }
    
     function tikrink_numeris(){
         
        var numeris = $('#numeris').val().length;
         
        if(!/^\d+(\.\d+)?/.exec($('#numeris').val()) || numeris != 9){
            $('#numeris_error').html("Rašykite: 86*******");
            $('#numeris_error').show();
            error_numeris = true;
        } else {
            $('#numeris_error').hide();
            
        }
    }
    
     function tikrink_slaptazodis(){
       
        var slaptazodis_ilgis = $("#slaptazodis").val().length;
        
        if(slaptazodis_ilgis < 5){
            $('#slaptazodis_error').html("Slaptažodis turėtų būti bent 5 simbolių");
            $('#slaptazodis_error').show();
            error_slaptazodis = true;
        } else {
            $('#slaptazodis_error').hide();
        }
    }
    
     function tikrink_slaptazodis2(){
       
        var slapta = $("#slaptazodis").val();
        var slapta2 = $("#slaptazodis2").val();
        
        if(slapta == slapta2){
            $('#slaptazodis2_error').hide();
        } else {
            $('#slaptazodis2_error').html("Slaptažodžiai nesutampa");
            $('#slaptazodis2_error').show();
            error_slaptazodis2 = true;
        }
    }
    
    
    $('#registracija').submit(function(){
            
            
    error_prisijungimoVardas = false;
    error_vardas = false;
    error_pavarde = false;
    error_pastas = false;
    error_numeris = false;
    error_slaptazodis = false;
    error_slaptazodis2 = false;
   
    
    
    tikrink_prisijungimoVardas();
    tikrink_vardas();
    tikrink_pavarde();
    tikrink_pastas();
    tikrink_numeris(); 
    tikrink_slaptazodis(); 
    tikrink_slaptazodis2();
  
    if(error_prisijungimoVardas == false && error_vardas == false && error_pavarde == false && error_pastas == false 
            && error_numeris == false && error_slaptazodis == false && error_slaptazodis2 == false){
    return true;
        } else {
            return false;
        }
    });
            
});


