<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class basket_model extends CI_Model{
    
    function _construct(){
        parent::__construct();
    }
    
    private $lastQuery = '';
    
	public function findAll($limit, $start){
            $this->db->limit($limit, $start);
            $query = $this->db->get('prekes');
            $this->lastQuery = $this->db->last_query();
            
            if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
            
        }
        
        public function findType(){
            $query = $this->db->get('prekes');
            if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
        }
        
        
        public function find($id){
            $this->db->where('id', $id);
            return $this->db->get('prekes')->row();
        }

        public function delete($id){
		$this->db->where('id', $id);
		$this->db->delete('prekes');
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}
        
        public function getTotalRow(){
            $sql = explode('LIMIT', $this->lastQuery);
            $query = $this->db->query($sql[0]);
            $result = $query->result();
            return count($result);
        }
}

