<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class admin_model extends CI_Model{
        
    public function getData(){
		$this->db->order_by('data', 'desc');
		$query = $this->db->get('naudotojas');
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
        
    public function getNauById($naudotojoID){
		$this->db->where('naudotojoID', $naudotojoID);
		$query = $this->db->get('naudotojas');
		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return false;
		}
	}
        
    public function delete($naudotojoID){
		$this->db->where('naudotojoID', $naudotojoID);
		$this->db->delete('naudotojas');
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}
    //--------?????
     public function atnaujinti(){
		$naudotojoID = $this->input->post('txt_hidden_id');
		$field = array(
			'prisijungimoVardas'=>$this->input->post('prisijungimoVardas'),
			'vardas'=>$this->input->post('vardas'),
                        'pavarde'=>$this->input->post('pavarde'),
                        'pastas'=>$this->input->post('pastas'),
                        'numeris'=>$this->input->post('numeris'),
                        'tipas'=>$this->input->post('tipas'),
                
			);
		$this->db->where('naudotojoID', $naudotojoID);
		$this->db->update('naudotojas', $field);
		
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}//-------------
        
        public function update(){
		$naudotojoID = $this->input->post('txt_hidden');
          
		$field = array(
			'prisijungimoVardas'=>$this->input->post('prisijungimoVardas'),
			'vardas'=>$this->input->post('vardas'),
                        'pavarde'=>$this->input->post('pavarde'),
                        'pastas'=>$this->input->post('pastas'),
                        'numeris'=>$this->input->post('numeris'),
                        'slaptazodis'=>$this->input->post('slaptazodis'),
			'data'=>date('Y-m-d H:i:s'),
                        'tipas'=>$this->input->post('tipas')
			);
		$this->db->where('naudotojoID', $naudotojoID);
		$this->db->update('naudotojas', $field);
            
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
            
	}
	
    
         public function zinutes(){
		
		$query = $this->db->get('zinute');
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
        
        public function atsiliepimai(){
		
		$query = $this->db->get('atsiliepimai');
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
        
        public function istrintiAts($atsiliepimoID){
		$this->db->where('atsiliepimoID', $atsiliepimoID);
		$this->db->delete('atsiliepimai');
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}
        
        public function istrintiZinute($zinutes_id){
		$this->db->where('zinutes_id', $zinutes_id);
		$this->db->delete('zinute');
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}
        
        
        
         public function uzsakymai(){
		$this->db->order_by('data', 'desc');
		$query = $this->db->get('uzsakymai');
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
        
          public function patvirtinta(){
		$this->db->order_by('data', 'desc');
		$query = $this->db->get('patvirtinta');
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
        
        
        
        public function status_update(){
		$uzsakymoNr = $this->input->post('txt_hidden_id');
		$field = array(
			
			'data'=>date('Y-m-d H:i:s'),
                        'busena'=>$this->input->post('busena')
			);
		$this->db->where('uzsakymoNr', $uzsakymoNr);
		$this->db->update('patvirtinta', $field);
		
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}

}

