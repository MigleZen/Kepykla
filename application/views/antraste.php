<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Cinamonas</title>
       
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url('css/bootstrap.min.css'); ?>" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo base_url('css/business-frontpage.css'); ?>" rel="stylesheet">
    <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>js/validation.js"></script>
    
    

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

     
                        
    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" style="background-color: #330000" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>         
                <a class="navbar-brand" href="<?php echo base_url(); ?>mainController/home">
                    <!--<span class="glyphicon glyphicon-home"></span>-->
                    <b>CINAMONAS </b></a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->         
            
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">   
                    
                     <li>
                        <a href="<?php echo base_url(); ?>mainController/apiekepyklele">Apie kepyklėlę</a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Gaminiai<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo base_url(); ?>basket/bandeles">Bandelės</a></li>
                        <li><a href="<?php echo base_url(); ?>basket/keksiukai">Keksiukai</a></li>
                    </ul>
                    </li>
                    <li>
                        <a href="<?php echo base_url(); ?>admin/atsiliepimuSarasas">Atsiliepimai</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url(); ?>mainController/kontaktai">Kontaktai</a>
                    </li>
              
                     
                 
                    <li>
                    <?php
                    if (isset($this->session->userdata['user_logged'])) { 
                            if($_SESSION['tipas'] == 0){
                                    echo '<a href="'.base_url().'user/naudotojas">JŪSŲ PASKYRA</a>';
                            }
                            else{
                                    echo '<a href="'.base_url().'admin">ADMINISTRATORIAUS PASKYRA</a>';
                            }  
                    }
                    ?>
                    </li>
                    <li>
                        <?php
                        if (isset($this->session->userdata['user_logged'])) {
                            echo '<a href="'.base_url().'auth/atsijungti"><b>ATSIJUNGTI</b></a>';
                        }
                        else {
                            echo '<a href="'.base_url().'auth/prisijungti"><b>PRISIJUNGTI</b></a>';
                        }
                        ?>
                   </li> 
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Image Background Page Header -->
    <!-- Note: The background image is set within the business-casual.css file. -->
    <header class="business-header">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                </div>
            </div>
        </div>
    </header>
    
    