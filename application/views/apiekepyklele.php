 <div class="container">
        
        <div class="row">
            <div class="col-sm-12"><br>
                <img class="img-circle img-responsive img-center img-style" src="<?php echo base_url(); ?>images/cupcakelogo.jpg" alt="Cupcake">
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-12">
                <h2 class="header-style">APIE KEPYKLĖLĘ</h2><br>
            </div>
        </div>
   
        <div class="row">
            <div class="col-sm-6">
                <h2 class="header-style">Gardūs naminiai kepiniai</h2>
                <hr class="brownhr">
                <p class="text-style">
                Esame kepyklėlė, įsikūrusi Baltupių rajone, Vilniuje. Mes rūpinamės savo klientais 
                ir stengiamės visada pateisinti jų lūkesčius. 
                Kiekvieną rytą minkome ir kildiname tešlą, rūpestingai formuojame ir kepame 
                Jums. Mūsų tikslas - pateikti Jums tik visada šviežius ir natūralius desertus, 
                pripildytus šilumos ir skonių įvairovės. 
                </p><br>
                <p class="text-style">
                Viskas gaminama iš natūralių žaliavų 
                pagal griežtas kepyklų amato taisykles. Naudojami pačios aukščiausios rūšies 
                miltai, migdolų milteliai, tikras šokoladas ir grietinėlė. Pasilepinkite pažįstamais 
                ir dar neragautais gardėsiais arba pradžiuginkite kitus smaližius. Tegu 
                kiekviena gera diena prasideda nuo Jūsų mėgstamiausios bandelės!
                </p>
            </div>
        
            <div class="row">
                <div class="col-sm-6">
                   <img  class="img-circle img-responsive img-center big-img-style" src="<?php echo base_url(); ?>images/kepykla.jpg" alt="">
                </div>
            </div>
        </div>
     
     <br>
     <br>
     <br>
     
     <div class="row">
               
        <div class="col-sm-6">
           <img  class="img-circle img-responsive img-center big-img-style" src="<?php echo base_url(); ?>images/bandeles.jpg" alt="">
        </div>
            
        <div class="row">
            <div class="col-sm-6">
                <h2 class="header-style">Mūsų vizija</h2>
                <hr class="brownhr">
                <p class="text-style">
                Siekiame tapti mėgstamiausia kepyklėle kiekvienam, ragaujančiam mūsų tiekiamus produktus.
                Norime palikti Jums neišdildomą įspūdį, todėl mūsų kepiniai visuomet švieži ir gaminami su
                meile. Siūlome jums paragauti įvairiausių sausainių, pyragų, bandelių bei keksiukų. Tobulindami 
                jau pamėgtus gaminius, plečiame savo asortimentą, taip siekdami, jog
                smaližiai visuomet grįžtų pas mus.
                </p><br>
                
            </div>
        </div>        
    </div>
 <br><br><br>
 </div>
