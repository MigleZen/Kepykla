    <div class="container">
        
        <div class="row">
            <div class="col-sm-12"><br>
                <img class="img-circle img-responsive img-center img-style" src="<?php echo base_url(); ?>images/cupcakelogo.jpg" alt="Cupcake">
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-12">
                <a href="<?php echo base_url(); ?>mainController/apiekepyklele"><h2 class="header-style">APIE KEPYKLĖLĘ</h2></a><br>
            </div>
        </div>
   
        <div class="row">
            <div class="col-sm-6">
                <h2 class="header-style">Gardūs naminiai kepiniai</h2>
                <hr class="brownhr">
                <p class="text-style">
                Esame kepyklėlė, įsikūrusi Baltupių rajone, Vilniuje. Mes rūpinamės savo klientais 
                ir stengiamės visada pateisinti jų lūkesčius. 
                Kiekvieną rytą minkome ir kildiname tešlą, rūpestingai formuojame ir kepame 
                Jums. Mūsų tikslas - pateikti Jums tik visada šviežius ir natūralius desertus, 
                pripildytus šilumos ir skonių įvairovės. 
                </p><br>
                <p class="text-style">
                Viskas gaminama iš natūralių žaliavų 
                pagal griežtas kepyklų amato taisykles. Naudojami pačios aukščiausios rūšies 
                miltai, migdolų milteliai, tikras šokoladas ir grietinėlė. Pasilepinkite pažįstamais 
                ir dar neragautais gardėsiais arba pradžiuginkite kitus smaližius. Tegu 
                kiekviena gera diena prasideda nuo Jūsų mėgstamiausios bandelės!
                </p>
                <br><br>
                <a href="<?php echo base_url('basket'); ?>" class="btn btn-default btn-block text-style" style="text-align: center"><b>Peržiūrėti produkciją</b></a><br>
                
            </div>
        
            <div class="row">
                <div class="col-sm-6">
                   <img  class="img-circle img-responsive img-center big-img-style" src="<?php echo base_url(); ?>images/kepykla.jpg" alt="">
                </div>
            </div>
        </div>
         
        
        
        <!-- /.row -->    
        
        <!--
        
        <div class="row">
            <div class="col-sm-12"><br>
                <img class="img-circle img-responsive img-center img-style" src="<?php echo base_url(); ?>images/cupcakelogo.jpg" alt="Cupcake">
            </div>
        </div>
    
        
        
        <div class="row">
            <div class="col-sm-12">
                <a href="<?php echo base_url(); ?>basket"><h2 class="header-style">GAMINIAI</h2></a>
                <br>             
                    <h2 class="header-style">Mūsų prekės</h2>
                   
                <br>
            </div>
        </div>
    <br>
    
    
    
    <div class="row">

        <div class="panel-style">
            <div class="panel-body">
            <br>
                <div class="col-md-4 col-lg-4 col-sm-4" >
                        <div class="panel-style2 thumbnail">
                            <img src="<?php echo base_url(); ?>images/bandele.jpg" alt="" style="width:150px;height:150px;">
                            <div class="caption">
                                <h4 class="pull-right text-style">0.50 €</h4>
                                <h4 class="text-style">Bandelė "Obelis"</h4>
                                </div>
                            <div class="ratings">
                                <!--
                                <center>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star-empty"></span>
                                </center> 
                                <br>
                            </div>
                        </div>
                  </div>
         
         <div class="col-sm-4 col-lg-4 col-md-4">
                        <div class="panel-style2 thumbnail">
                            <img src="<?php echo base_url(); ?>images/keksiukas3.png" alt="" style="width:150px;height:150px;">
                            <div class="caption">
                                <h4 class="pull-right text-style">1.53 €</h4>
                                <h4 class="text-style">Keksiukas su belgišku šokoladu</h4>                        
                                </div>
                            <div class="ratings">
                                <!--
                                <center>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star-empty"></span>
                                </center>
                                
                                <br>
                            </div>
                        </div>
                    </div>
         
         
         <div class="col-sm-4 col-lg-4 col-md-4">
                        <div class="panel-style2 thumbnail">
                            <img src="<?php echo base_url(); ?>images/bandele3.jpg" alt="" style="width:150px;height:150px;">
                            <div class="caption">
                                <h4 class="pull-right text-style">0.60 €</h4>
                                <h4 class="text-style">Karamelinė bandelė</h4>
                                </div>
                            <div class="ratings">
                                <!--
                                <center>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                </center>
                                
                                
                                <br>
                            </div>
                        </div>
                  </div>
      
            </div>
        </div>
    </div>
    <br>
    -->
        <!-- /.row -->
        
        <div class="row">
            <div class="col-sm-12">
                <br>
                <img class="img-circle img-responsive img-center img-style" src="<?php echo base_url(); ?>images/cupcakelogo.jpg" alt="Cupcake">
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-12">
                <a href="<?php echo base_url(); ?>mainController/kontaktai"><h2 class="header-style">KONTAKTAI</h2></a>
                <br>
                <br>
          </div>      
        </div>
        
        <div class="row">
            <!-- Map Column -->
            <div class="col-md-5 col-md-offset-1">
                <!-- Embedded Google Map -->
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2303.8663888943165!2d25.26143631555337!3d54.7295649802921!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46dd913fbd59ec69%3A0xb02b53c89c44c103!2sDidlaukio+g.+47%2C+Vilnius+08303!5e0!3m2!1slt!2slt!4v1488629553873" 
                        width="400" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
            <!-- Contact Details Column -->
            
            <div class="col-md-5">
                
                <p class="text-style">
                    <b>Adresas:</b><br> 
                    Didlaukio g. 47,<br> Vilnius LT-08303<br>
                </p>
                <p class="text-style">
                    <b>Telefonas:</b> (8-5) 000 0000</p>
                <p class="text-style"> 
                    <b>Elektroninis paštas</b>: <a href="mailto:info@cinamonas.lt">info@cinamonas.lt
                    </a>
                </p>
                <p class="text-style">
                    <b>Darbo laikas</b> <br>
                    Pirmadienis - Penktadienis 7:00 - 19:00 val.<br>
                    Šeštadienis - Sekmadienis 8:00 - 15.00 val.
                </p>
                <br>
                <a href="<?php echo base_url('mainController/kontaktai'); ?>" style="text-align: center">Susisiekite su mumis čia</a><br>
                
            </div>
        </div>  

        <br><br><br>
        
    </div>