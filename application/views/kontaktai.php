<div class="container">

    <div class="row">
            <div class="col-sm-12"><br>
                <img class="img-circle img-responsive img-center img-style" src="<?php echo base_url(); ?>images/cupcakelogo.jpg" alt="Cupcake">
                <br>
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-12">
                <h2 class="header-style">KONTAKTAI</h2>
                <br>
                <br>
          </div>      
        </div>
        
        <div class="row">
            <!-- Map Column -->
            <div class="col-md-5 col-md-offset-1">
                <!-- Embedded Google Map -->
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2303.8663888943165!2d25.26143631555337!3d54.7295649802921!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46dd913fbd59ec69%3A0xb02b53c89c44c103!2sDidlaukio+g.+47%2C+Vilnius+08303!5e0!3m2!1slt!2slt!4v1488629553873" 
                        width="400" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
            <!-- Contact Details Column -->
            
            <div class="col-md-5">
                
                <p class="text-style">
                    <b>Adresas:</b><br> 
                    Didlaukio g. 47,<br> Vilnius LT-08303<br>
                </p>
                <p class="text-style">
                    <b>Telefonas:</b> (8-5) 000 0000</p>
                <p class="text-style"> 
                    <b>Elektroninis paštas</b>: <a href="mailto:info@cinamonas.lt">info@cinamonas.lt
                    </a>
                </p>
                <p class="text-style">
                    <b>Darbo laikas</b> <br>
                    Pirmadienis - Penktadienis 7:00 - 19:00 val.<br>
                    Šeštadienis - Sekmadienis 8:00 - 15.00 val.
                </p>
            </div>
        </div>
    
    <!-- Contact Form -->
        <!-- In order to set the email address and subject line for the contact form go to the bin/contact_me.php file. -->
        <div class="row">
            <br><br>
            <div class="col-md-8 col-md-offset-2">
                <?php if(isset($_SESSION['success'])) { ?>
                    <div class="alert alert-success"> <?php echo $_SESSION['success']; ?></div>
                <?php }
                ?>
                    
                <?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>
                 
                <br><br>
                <h3 class="header-style">Parašykite mums</h3><br>
                <form name="sentMessage" id="siusti" action="<?php echo base_url(); ?>mainController/kontaktai" method="POST">
                    <div class="form-group text-style">
                   <label for="kvardas">Jūsų vardas</label>
                   <input class="form-control" name="kvardas" id="kvardas" type="text" required=""> 
                </div>
                <div class="form-group text-style">
                   <label for="kpastas">Jūsų el. pašto adresas</label>
                   <input class="form-control" name="kpastas" id="kpastas" type="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" /> 
                </div>
                <div class="form-group text-style">
                   <label for="zinute">Jūsų žinutė</label>
                   <textarea class="form-control" name="zinute" id="zinute" type="text" cols="40" rows="4" placeholder="Įveskite savo žinutę..." required=""></textarea>
                </div>
                <div class="center">
                    <button class="btn btn-danger" name="siusti">Siųsti</button>
                </div>
            
                 </form>    
            </div>
        </div>
       <br><br><br>
</div>    
        <!-- /.row -->

