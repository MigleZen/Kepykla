<div class="container">
    
    <div class="row">
            <div class="col-sm-12">
                <br>
                <img class="img-circle img-responsive img-center img-style" src="<?php echo base_url(); ?>images/cupcakelogo.jpg" alt="Cupcake">
            </div>
        </div>
        
    <div class="row">
        <div class="col-sm-12">
            <br>
           
            <h2 class="header-style">Užsakymų istorija</h2>
            <br>
            <br>
        </div>
    </div>
   
    
    <div class="row">
        <div class="col-sm-12">
          
            <a  href="<?php echo base_url('user/naudotojas'); ?>" class="btn btn-default pull-right">Grįžti</a>
            <br><br>
            
	<table class="table table-bordered table-responsive">
		<thead>
			<tr>
                                
                                <th>Užsakymo numeris</th>
                                <th>Prekių kiekis</th>
				<th>Užsakymo kaina</th>
				<th>Data</th>
                                <th>Būsena</th>
			</tr>
		</thead>
		<tbody>
		<?php 
                
			if($uzsakymai){
				foreach($uzsakymai as $uzs){
		?>
			<tr>
                            <?php if($_SESSION['naudotojoID'] == $uzs->naudotojoID){   ?>
                                
                                <td><?php echo $uzs->uzsakymoNr; ?></td>
                                <td><?php echo $uzs->prekiuKiekis; ?></td>
				<td><?php echo $uzs->uzsakymoKaina; ?> Eur</td>
                                <td><?php echo $uzs->data; ?></td>
                                <td><?php echo $uzs->busena; ?></td>
                                <?php }?>    
                                
			</tr>
		<?php
				}
			}
		?>
		</tbody>
	</table>
            
        </div>
    </div>
</div>
<br><br>
