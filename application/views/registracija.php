<div class="container">
<div class="row">
            <div class="col-sm-12">
                <br>
                <img class="img-circle img-responsive img-center img-style" src="<?php echo base_url(); ?>images/cupcakelogo.jpg" alt="Cupcake">
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-12">
                <h2 class="header-style">REGISTRACIJA</h2>
                <br>
                <br>
          </div>      
        </div>
        
    
<?php 
//
//mysqli_connect("localhost", "root", "", "cinamono_db");
//
//if(isset($_POST['pvardas'])){
//    
//    $pvardas = mysqli_real_escape_string($_POST['pvardas']);
//    
//    if(!empty($pvardas)){
//      $query = mysqli_query("SELECT * FROM naudotojai WHERE pvardas='$pvardas'");
//      $pvardas_result = mysqli_num_rows($query);
//      
//      if($pvardas_result == 1){
//          echo "Tinkamas prisijungimo vardas";
//      } else {
//          echo "Toks naudotojas jau yra";
//      }
//    }
//}


?>
    
        <div class="row">

            
            <div class="col-md-6 col-md-offset-3">
                <?php if(isset($_SESSION['success'])) { ?>
                    <div class="alert alert-success"> <?php echo $_SESSION['success']; ?></div>
                <?php }
                ?>
                    
                <?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>
                    
                 <form id=registracija name='form' action="<?php echo base_url(); ?>auth/registracija" method="POST">
                     
                <div class="form-group text-style">
                   <label for="prisijungimoVardas">Prisijungimo vardas</label>
                   <input class="form-control form_text" name="prisijungimoVardas" id="prisijungimoVardas" type="text" >
                   <span class="error_form" id="prisijungimoVardas_error"></span>
<!--                   <div id="status"></div>-->
                </div>
                <div class="form-group text-style">
                   <label for="vardas">Vardas</label>
                   <input class="form-control" name="vardas" id="vardas" type="text" > 
                   <span class="error_form" id="vardas_error"></span>
                </div>
                <div class="form-group text-style">
                   <label for="pavarde">Pavardė</label>
                   <input class="form-control" name="pavarde" id="pavarde" type="text" >
                   <span class="error_form" id="pavarde_error"></span>
                </div>
                <div class="form-group text-style">
                   <label for="pastas">El.paštas</label>
                   <input class="form-control" name="pastas" id="pastas" type="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" /> 
                   <span class="error_form" id="pastas_error"></span>
                </div>
                <div class="form-group text-style">
                   <label for="numeris">Numeris</label>
                   <input class="form-control" name="numeris" id="numeris" type="text" > 
                   <span class="error_form" id="numeris_error"></span>
                </div>
                <div class="form-group text-style">
                   <label for="slaptazodis">Slaptažodis</label>
                   <input class="form-control" name="slaptazodis" id="slaptazodis" type="password" >
                   <span class="error_form" id="slaptazodis_error"></span>
                </div>
                <div class="form-group text-style">
                   <label for="slaptazodis">Pakartoti slaptažodį:</label>
                   <input class="form-control" name="slaptazodis2" id="slaptazodis2" type="password"> 
                   <span class="error_form" id="slaptazodis2_error"></span>
                </div>
                <div class="center">
                    <br>
                    <button class="btn btn-danger" name="registracija" >Registruotis</button>
                    
                </div>
            
                 </form>    
            </div>       
        </div>
        <br><br><br>

</div>