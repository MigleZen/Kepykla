<div class="container">
<div class="row">
            <div class="col-sm-12">
                <br>
                <img class="img-circle img-responsive img-center img-style" src="<?php echo base_url(); ?>images/cupcakelogo.jpg" alt="Cupcake">
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-12">
                <h2 class="header-style">PRISIJUNGIMAS</h2>
                <br>
                <br>
          </div>      
        </div>
                  
    
        <div class="row">         
            <div class="col-md-6 col-md-offset-3">
                
                <?php if(isset($_SESSION['success'])) { ?>
                    <div class="alert alert-success"> <?php echo $_SESSION['success']; ?></div>
                <?php }
                ?>
                
                <?php if (isset($_SESSION['error'])) { ?>
                    <div class="alert alert-danger"> <?php echo $_SESSION['error']; ?></div>
                <?php
                } ?>    
                    
                <?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>
                 <form action="" method="POST">
            
                <div class="form-group text-style">
                   <label for="pastas">Prisijungimo vardas</label>
                   <input class="form-control" name="prisijungimoVardas" id="prisijungimoVardas" type="text" > 
                </div>
                
                <div class="form-group text-style">
                   <label for="slaptazodis">Slaptažodis</label>
                   <input class="form-control" name="slaptazodis" id="slaptazodis" type="password" > 
                </div>
                
                <div class="center">
                    <button class="btn btn-danger" name="prisijungti">Prisijungti</button>
                    <a href="<?php echo base_url(); ?>auth/registracija" class="pull-right text-style">Registruokitės čia</a><br>
                </div>  
                </form>  
                    <br><br><br>
            </div>
            
        </div>
    </div>

        

