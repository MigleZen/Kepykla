<div class="container">
    
    <div class="row">

        <div class="row">
            <div class="col-sm-12">
                <br><br>
                <h2 class="header-style">Redaguoti naudotojo informaciją</h2>
                <br><br>
            </div>
        </div>


        <div class="col-md-6 col-md-offset-3">
	<form action="<?php echo base_url('admin/update') ?>" method="post" class="form-horizontal">
            <input type="hidden" name="txt_hidden" value="<?php echo $nau->naudotojoID; ?>">
            
		

        <div class="form-group text-style">
                   <label for="prisijungimoVardas">Prisijungimo vardas</label>
                   <input class="form-control" value="<?php echo $nau->prisijungimoVardas; ?>" name="prisijungimoVardas" type="text" required=""> 
                </div>
                <div class="form-group text-style">
                   <label for="vardas">Vardas</label>
                   <input class="form-control" value="<?php echo $nau->vardas; ?>" name="vardas" type="text" required=""> 
                </div>
                <div class="form-group text-style">
                   <label for="pavarde">Pavardė</label>
                   <input class="form-control" value="<?php echo $nau->pavarde; ?>" name="pavarde" type="text" required=""> 
                </div>
                <div class="form-group text-style">
                   <label for="pastas">El.paštas</label>
                   <input class="form-control" value="<?php echo $nau->pastas; ?>" name="pastas" type="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" /> 
                </div>
                <div class="form-group text-style">
                   <label for="numeris">Numeris</label>
                   <input class="form-control" value="<?php echo $nau->numeris; ?>" name="numeris" type="text" required="" pattern="^(([0-9]*)|(([0-9]*)\.([0-9]*)))$" maxlength="9" minlength="9"> 
                </div>
            <!--
                <div class="form-group text-style">
                   <label for="numeris">Slaptažodis</label>
                   <input class="form-control" value="<?php echo $nau->slaptazodis; ?>" name="slaptazodis" type="password" readonly> 
                </div>
            
            
                 <div class="form-group text-style">
                   <label for="tipas">Tipas</label>
                   <input class="form-control" value="<?php echo $nau->tipas; ?>" name="tipas" type="text" required=""> 
                </div>
            -->
                 <div class="form-group text-style">
                <label for="tipas">Tipas</label>
                <select class="form-control" name="tipas">
                <option value="0">Paprastas naudotojas</option>
                <option value="1">Administratorius</option>
                </select>
            </div>
                
                <div class="form-group">
			
                    <input type="submit" name="btnUpdate" class="btn btn-danger" value="Atnaujinti">
                    <a href="<?php echo base_url('admin/admin_naud'); ?>" class="btn btn-default">Grįžti</a>
			
		</div>
            
                 </form>
            <br><br><br>
        </div>
    </div>
    
</div>