<div class="container">
    
    <div class="row">
            <div class="col-sm-12">
                <br>
                <img class="img-circle img-responsive img-center img-style" src="<?php echo base_url(); ?>images/cupcakelogo.jpg" alt="Cupcake">
            </div>
        </div>
        
    <div class="row">
        <div class="col-sm-12">
            <br>
           
            <h2 class="header-style">Tvarkyti užsakymų būsenas</h2>
            <br>
            <br>
        </div>
    </div>
   
    <?php
		if($this->session->flashdata('success_msg')){
	?>
		<div class="alert alert-success">
			<?php echo $this->session->flashdata('success_msg'); ?>
		</div>
	<?php		
		}
	?>
    
    <?php
		if($this->session->flashdata('error_msg')){
	?>
		<div class="alert alert-success">
			<?php echo $this->session->flashdata('error_msg'); ?>
		</div>
	<?php		
		}
	?>
    
    <div class="row">
        <div class="col-sm-12">
          
            <a  href="<?php echo base_url('admin'); ?>" class="btn btn-default pull-right">Grįžti</a>
            <br><br>
            
	<table class="table table-bordered table-responsive">
		<thead>
			<tr>
                                
                                <th>Užsakymo numeris</th>
                                <th>Prekių kiekis</th>
				<th>Užsakymo kaina</th>
				<th>Data</th>
                                <th>Būsena</th>
                                <th>Atnaujinti</th>
			</tr>
		</thead>
		<tbody>
		<?php 
                
			if($uzsakymai){
				foreach($uzsakymai as $uzs){
		?>
			<tr>
                            
                                
                                <td><?php echo $uzs->uzsakymoNr; ?></td>
                                <td><?php echo $uzs->prekiuKiekis; ?></td>
				<td><?php echo $uzs->uzsakymoKaina; ?> Eur</td>
                                <td><?php echo $uzs->data; ?></td>
                                <td><?php echo $uzs->busena; ?></td>
                           
                                
                                      <td>
                                    
                                    
             <form action="<?php echo base_url('admin/status_update') ?>" method="post" class="form-horizontal">
            <input type="hidden" name="txt_hidden_id" value="<?php echo $uzs->uzsakymoNr; ?>">
            
               
            <div class="form-group text-style">
                <select class="form-control"  name="busena">
                <option value="Užsakyta">Užsakyta</option>
                <option value="Gaminama">Gaminama</option>
                <option value="Pagaminta">Pagaminta</option>
                </select>
            </div>
            
            <!--
                <div class="form-group text-style">
                   
                   <input class="form-control" value="<?php echo $uzs->busena; ?>" name="busena" type="text" required> 
                </div>
              -->
                
                <div class="form-group">
			
                   <input type="submit" name="btnUpdate" class="btn btn-danger" value="Atnaujinti">
			
		</div>
            
                 </form>
                                    
                                    
                                </td>
                                
			</tr>
		<?php
				}
			}
		?>
		</tbody>
	</table>
            
        </div>
    </div>
</div>
<br><br>
