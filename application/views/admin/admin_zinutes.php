<div class="container">
        
    <div class="row">
            <div class="col-sm-12"><br>
                <img class="img-circle img-responsive img-center img-style" src="<?php echo base_url(); ?>images/cupcakelogo.jpg" alt="Cupcake">
            </div>
        </div>
    
    <div class="row">
        <div class="col-sm-12">
            
            <h2 class="header-style">Žinutės</h2>
           <br>
           <br>
        </div>
    </div>
    
    <?php
		if($this->session->flashdata('success_msg')){
	?>
		<div class="alert alert-success">
			<?php echo $this->session->flashdata('success_msg'); ?>
		</div>
	<?php		
		}
	?>
    
    <?php
		if($this->session->flashdata('error_msg')){
	?>
		<div class="alert alert-success">
			<?php echo $this->session->flashdata('error_msg'); ?>
		</div>
	<?php		
		}
	?>
    
    <div class="row">
        <div class="col-sm-12">
          
	<table class="table table-bordered table-responsive">
		<thead>
			<tr>
				<td>ID</td>
				<th>Vardas</th>
                                <th>El. paštas</th>
				<th>Žinutė</th>
                                <th>Data</th>
				
                                <th>Veiksmas</th>
                               
			</tr>
		</thead>
		<tbody>
		<?php 
			if($zinutes){
				foreach($zinutes as $zinute){
		?>
			<tr>
				<td><?php echo $zinute->zinutes_id; ?></td>
				<td><?php echo $zinute->kvardas; ?></td>
				<td><?php echo $zinute->kpastas; ?></td>
				<td><?php echo $zinute->zinute; ?></td>
                                <td><?php echo $zinute->data; ?></td>
                               
                                <td>
                                <a href="<?php echo base_url('admin/istrintiZinute/'.$zinute->zinutes_id); ?>" class="btn btn-danger" onclick="return confirm('Ar tikrai norite ištrinti žinutę?');">Ištrinti</a>
                                </td>
			</tr>
		<?php
				}
			}
		?>
		</tbody>
	</table>
            
        </div>
    </div>
<br><br><br>
</div>