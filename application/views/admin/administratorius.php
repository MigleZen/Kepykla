<div class="container">
        
    <div class="row">
            <div class="col-sm-12"><br>
                <img class="img-circle img-responsive img-center img-style" src="<?php echo base_url(); ?>images/cupcakelogo.jpg" alt="Cupcake">
            </div>
        </div>
    
        <div class="row">
            <div class="col-sm-12">
               
                <br>
                <h2 class="header-style">Administratoriaus paskyra</h2>
                <br>
                <br>
          </div>      
        </div>
    
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                  <br>
                
                <h3 class="text-style">Pasirinkite, ką norite vykdyti: </h3>
                <br>
                <br>
                
            </div>       
        </div> 
        
    
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                   
                    <a href="<?php echo base_url(); ?>basket" class="text-style"><h4><b>- PERŽIŪRĖTI KEPINIUS</b></h4></a><br>
                    <a href="<?php echo base_url(); ?>admin/admin_kepiniai" class="text-style"><h4><b>- PRIDĖTI KEPINIUS</b></h4></a><br>
                    <a href="<?php echo base_url(); ?>admin/zinutes" class="text-style"><h4><b>- PERŽIŪRĖTI ŽINUTES</b></h4></a><br>
                    <a href="<?php echo base_url(); ?>admin/admin_naud" class="text-style"><h4><b>- PERŽIŪRĖTI NAUDOTOJUS</b></h4></a><br>
                    <a href="<?php echo base_url(); ?>admin/atsiliepimuSarasas" class="text-style"><h4><b>- PERŽIŪRĖTI ATSILIEPIMUS</b></h4></a><br>
                    <a href="<?php echo base_url(); ?>admin/admin_uzsakymai" class="text-style"><h4><b>- PERŽIŪRĖTI UŽSAKYMUS</b></h4></a><br>
                    <a href="<?php echo base_url(); ?>admin/admin_busenos" class="text-style"><h4><b>- TVARKYTI UŽSAKYMŲ BŪSENAS</b></h4></a><br>
                    
                    <br>
                <br>
                <br>
                <br>
            </div>       
        </div> 
</div>
<br>
<br>