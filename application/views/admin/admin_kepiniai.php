<div class="container">
    <div class="row">
            <div class="col-sm-12"><br>
                <img class="img-circle img-responsive img-center img-style" src="<?php echo base_url(); ?>images/cupcakelogo.jpg" alt="Cupcake">
            </div>
        </div>
    
    <div class="row">
        
        
        
    
<?php

    
    $msg = "";
    // jei upload paspaustas
    if(isset($_POST['upload'])){
        $this->form_validation->set_rules('type', 'Tipas', 'required|numeric|exact_length[1]');
        $this->form_validation->set_rules('price', 'Kaina', 'required|numeric');
        if($this->form_validation->run() == TRUE){
        
        $target = "images/".basename($_FILES['image']['name']);
       
        $db = mysqli_connect("localhost", "root", "", "cinamono_db");
        
        $name = $_POST['name'];
        $image = $_FILES['image']['name'];
        $text = $_POST['text'];
        $price = $_POST['price'];
        $type = $_POST['type'];
        
        $sql = "INSERT INTO prekes (name, image, text, price, type) VALUES ('$name', '$image', '$text', '$price', '$type')";
        mysqli_query($db, $sql); 
        
        
        if(move_uploaded_file($_FILES['image']['tmp_name'], $target)) {
            $this->session->set_flashdata("success", "Gaminys sėkmingai išsaugotas.");
            redirect("admin/admin_kepiniai", "refresh");
              // $msg = "Nuotrauka sėkmingai įkelta";
        }else{
            $this->session->set_flashdata("success", "Kilo problema išsaugojant gaminį.");
            redirect("admin/admin_kepiniai", "refresh");
              //  $msg = "Kilo problema įkeliant nuotrauką";
        }
        
        }
    }

?>
        

        <div class="row">
            <div class="col-sm-12">
                <br>
            
                <h2 class="header-style">Pridėti kepinius</h2>
                <br>
                <br>
          </div>      
        </div>
        

        <div class="col-sm-6 col-md-offset-3">
        <div>
            
       <?php if(isset($_SESSION['success'])) { ?>
                    <div class="alert alert-success"> <?php echo $_SESSION['success']; ?></div>
                <?php }
                ?>
                    
                <?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>
                        
            <br>            
          <form method="post" action="admin_kepiniai" enctype="multipart/form-data">
            <input type="hidden" name="size" value="1000000">
            <div>
                <h5 class="form-group text-style">Įveskite pavadinimą</h5>
                <input class="form-control" type="text" name="name" maxlength='30' required="">
            </div>
            <div class="form-group text-style"><br>
                <h5>Pasirinkite paveikslėlį</h5><br>
                <input type="file" name="image"><br>
            </div>
            <div class="form-group text-style">
                <h5>Įveskite aprašymą</h5>
                <textarea class="form-control" name="text" cols="40" rows="4" placeholder="" required=""></textarea><br>
            </div>
            <div class="form-group text-style">
                <h5>Įveskite kainą</h5>
                <input class="form-control" type="text" name="price" pattern="\d+(\.\d{2})?||\d+(\.\d{1})?" required="">
            </div>
            
            <!--
            <div class="form-group text-style">
                <h5>Pasirinkite prekės tipą</h5>
                <input class="form-control" type="text" name="type" required="">
            </div> -->
            
            <div class="form-group text-style">
                <h5>Pasirinkite prekės tipą</h5>
                <select class="form-control" name="type">
                <option value="0">Bandelė</option>
                <option value="1">Keksiukas</option>
                </select>
            </div>
         
            <div>
                <br>
                <input class="btn btn-danger" type="submit" name="upload" value="Įkelti prekę">
                <a  href="<?php echo base_url('admin'); ?>" class="btn btn-default pull-right">Grįžti</a>
            </div>
        </form>
      </div>
    </div>
    </div>
     </div>
<br><br>