<div class="container">
        
    <div class="row">
            <div class="col-sm-12"><br>
                <img class="img-circle img-responsive img-center img-style" src="<?php echo base_url(); ?>images/cupcakelogo.jpg" alt="Cupcake">
            </div>
        </div>
    
    <div class="row">
        <div class="col-sm-12">
            <br>
            <br>
            <h2 class="header-style">Užsakymai</h2>
            <br>
            <br>
        </div>
    </div>
    
    <?php
		if($this->session->flashdata('success_msg')){
	?>
		<div class="alert alert-success">
			<?php echo $this->session->flashdata('success_msg'); ?>
		</div>
	<?php		
		}
	?>
    
    <?php
		if($this->session->flashdata('error_msg')){
	?>
		<div class="alert alert-success">
			<?php echo $this->session->flashdata('error_msg'); ?>
		</div>
	<?php		
		}
	?>
    
    <div class="row">
        <div class="col-sm-12">
          
            <a  href="<?php echo base_url('admin'); ?>" class="btn btn-default pull-right">Grįžti</a><br><br>
        
	<table class="table table-bordered table-responsive">
		<thead>
			<tr>
                                <th>Užsakymo Nr.</th>
                                <th>Naudotojo ID</th>
				<th>Prekės ID</th>
                                <th>Prekės pavadinimas</th>
				<th>Vienetai</th>
				<th>Data</th>
                                
                                
			</tr>
		</thead>
		<tbody>
		<?php 
			if($uzsakymai){
				foreach($uzsakymai as $uzs){
		?>
			<tr>
                                <td><?php echo $uzs->uzsakymoNr; ?></td>
                                <td><?php echo $uzs->naudotojoID; ?></td>
				<td><?php echo $uzs->prekes_id; ?></td>
                                <td><?php echo $uzs->prekes_pav; ?></td>
				<td><?php echo $uzs->vnt; ?></td>
                                <td><?php echo $uzs->data; ?></td>
                                
			</tr>
		<?php
				}
			}
		?>
		</tbody>
	</table>
            
        </div>
    </div>
    </div>
<br><br>

