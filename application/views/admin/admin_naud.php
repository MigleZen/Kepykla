<div class="container">
    <div class="row">
            <div class="col-sm-12"><br>
                <img class="img-circle img-responsive img-center img-style" src="<?php echo base_url(); ?>images/cupcakelogo.jpg" alt="Cupcake">
            </div>
        </div>
        
    <div class="row">
        <div class="col-sm-12">
            <br>
            <br>
            <h2 class="header-style">Naudotojai</h2>
            <br>
            <br>
        </div>
    </div>
    
    <?php
		if($this->session->flashdata('success_msg')){
	?>
		<div class="alert alert-success">
			<?php echo $this->session->flashdata('success_msg'); ?>
		</div>
	<?php		
		}
	?>
    
    <?php
		if($this->session->flashdata('error_msg')){
	?>
		<div class="alert alert-danger">
			<?php echo $this->session->flashdata('error_msg'); ?>
		</div>
	<?php		
		}
	?>
    
    
    <div class="row">
        <div class="col-sm-12">
        
       
	<table class="table table-bordered table-responsive">
		<thead>
			<tr>
				<td>ID</td>
				<th>Prisijungimo vardas</th>
				<th>Vardas</th>
				<th>Pavardė</th>
				<th>El. paštas</th>
                                <th>Numeris</th>
                                <th>Data</th>
                                
                                <th>Veiksmas</th>
			</tr>
		</thead>
		<tbody>
		<?php 
			if($naud){
				foreach($naud as $nau){
                                    if($nau->tipas == 0){
		?>
			<tr>
				<td><?php echo $nau->naudotojoID; ?></td>
				<td><?php echo $nau->prisijungimoVardas; ?></td>
				<td><?php echo $nau->vardas; ?></td>
				<td><?php echo $nau->pavarde; ?></td>
                                <td><?php echo $nau->pastas; ?></td>
                                <td><?php echo $nau->numeris; ?></td>
                                <td><?php echo $nau->data; ?></td>
                                
				<td>
					<a href="<?php echo base_url('admin/edit/'.$nau->naudotojoID); ?>" class="btn btn-info">Redaguoti</a><br>
                                        <p></p>
					<a href="<?php echo base_url('admin/delete/'.$nau->naudotojoID); ?>" class="btn btn-danger" onclick="return confirm('Ar norite ištrintį duomenis?');">Ištrinti</a>
				</td>
			</tr>
		<?php
                                    }
				}
			}
		?>
		</tbody>
	</table>
            <a  href="<?php echo base_url('admin'); ?>" class="btn btn-default pull-right">Grįžti</a>
        <br><br><br> 
        </div>
    </div>
</div>