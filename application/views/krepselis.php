<div class="container">
        <div class="row">
            <div class="col-sm-12">
                <br>
                <img class="img-circle img-responsive img-center img-style" src="<?php echo base_url(); ?>images/cupcakelogo.jpg" alt="Cupcake">
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-12">
                <br><br>
                <h2 class="header-style">KREPŠELIS</h2>
                <br><br>
            </div>
        </div>

  <div class="row">
            <div class="col-sm-12">
                
<?php echo anchor('basket', '<< Tęsti apsipirkimą'); ?> <br>
<br>
<?php echo form_open('shoppingcart/update'); ?>

<table class="table table-bordered table-responsive">
		<thead class="text-style">
            <tr>
                    <th>Pašalinti</th>
                    <th>Kiekis</th>
                    <th>Pavadinimas</th>
                    <th>Aprašymas</th>
                    <th>Vieneto kaina</th>
                    <th>Pilna kaina</th>
            </tr>
        </thead>
 <tbody class="text-style">
<?php $i = 1; ?>

<?php foreach ($this->cart->contents() as $items): ?>

        <?php echo form_hidden($i.'[rowid]', $items['rowid']); ?>

        <tr>
            <td align="center"><?php echo anchor('shoppingcart/delete/'.$items['rowid'], 'X'); ?></td>
                <td><?php echo form_input(array('name' => 'qty'.$i, 'value' => $items['qty'], 'maxlength' => '2', 'size' => '4')); ?></td>
                <td>
                        <?php echo $items['name']; ?>

                        <?php if ($this->cart->has_options($items['rowid']) == TRUE): ?>

                                <p>
                                        <?php foreach ($this->cart->product_options($items['rowid']) as $option_name => $option_value): ?>

                                                <strong><?php echo $option_name; ?>:</strong> <?php echo $option_value; ?><br />
 
                                        <?php endforeach; ?>
                                </p>

                        <?php endif; ?>

                </td>
                <td><?php echo $items['text']; ?></td>
                <td style="text-align:right"><?php echo $this->cart->format_number($items['price']); ?>€</td>
                <td style="text-align:right"><?php echo $this->cart->format_number($items['subtotal']); ?>€</td>
        </tr>

<?php $i++; ?>

<?php endforeach; ?>

<tr>
        <td colspan="4"> </td>
        <td class="right"><strong>Iš viso</strong></td>
        <td class="right"><?php echo $this->cart->format_number($this->cart->total()); ?>€</td>
</tr>
    </tbody>
</table>



<btn><?php echo form_submit('', 'Atnaujinti'); ?></btn>


<a class="pull-right" href="<?php echo base_url('shoppingcart/order/'); ?>" onclick="return confirm('Ar tikrai norite užsisakyti?')"><h4>Užsakyti</h4><br></a>
                                      

<?php form_close(); ?>

</div>
      </div>
</div>
<br><br>