<div class="container">

    <div class="row">
            <div class="col-sm-12"><br>
                <img class="img-circle img-responsive img-center img-style" src="<?php echo base_url(); ?>images/cupcakelogo.jpg" alt="Cupcake">
                <br>
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-12">
                <h2 class="header-style">ATSILIEPIMAI</h2>
                <br>
              
          </div>      
        </div>
        
    
        <div class="row">
            <br><br>
            <div class="col-md-5">
                <?php if(isset($_SESSION['success'])) { ?>
                    <div class="alert alert-success"> <?php echo $_SESSION['success']; ?></div>
                <?php }
                ?>
                    
                <?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>
                 
                <br>
                <h4 class="text-style" style="text-align: center"><b>Parašykite atsiliepimą</b></h4><br>
                <form  name="sentMessage" id="issiusti" action="<?php echo base_url(); ?>mainController/atsiliepimai" method="POST">
                    <div class="form-group text-style">
                   <label for="avardas">Jūsų vardas</label>
                   <input class="form-control" name="avardas" id="avardas" type="text" required=""> 
                </div>
                <div class="form-group text-style">
                   <label for="zinute">Jūsų atsiliepimas</label>
                   <textarea class="form-control" name="atsiliepimas" id="atsiliepimas" type="text" cols="50" rows="5" placeholder="Įveskite čia..." required=""></textarea>
                </div>
                <div class="center">
                    <button class="btn btn-danger" name="issiusti">Siųsti</button>
                </div>
            
                 </form>    
            </div>
            
            <div class="col-md-5 col-md-offset-1" id="ajax-data">
                <br>
                <table class="table table-bordered table-responsive pull-left">
		
		<tbody class="text-style">
                 
		<?php 
			if($atsiliepimai){
				foreach($atsiliepimai as $ats){
                                   
		?>
                       <tr>
                               
				<td class="hrstyle" style="border-color: white"><?php echo "<p>"."<br>"."<b>".$ats->avardas."</b>"."</p>"."<p>".$ats->data."</p>"."<br>"."<p>".$ats->atsiliepimas."</p>"; ?></td>
                       		
                		<?php     
                
                if (isset($this->session->userdata['user_logged'])) { 
                            if($_SESSION['tipas'] == 1){
                
                ?>
				<td style="border-color: white">
					<a  href="<?php echo base_url('admin/istrintiAts/'.$ats->atsiliepimoID); ?>" class="btn btn-primary pull-right" onclick="return confirm('Ar tikrai norite ištrinti gaminį?')">Ištrinti</a><br>
                                        <p></p>
					
				</td>
                 
                                <?php }                     
                }
                ?>
                                                
			</tr>
		<?php
                                    
				}
			}
		?>
		</tbody>
	</table>
                                       
                
            </div>
            
        </div>
       <br><br><br>
</div>  

        <!-- /.row -->

