<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller{

	function __construct(){
		parent:: __construct();
                $this->load->model('basket_model', 'bm');
                $this->load->model('admin_model', 'm');
	}

	function index(){
        if (isset($this->session->userdata['user_logged'])) { 
                if($_SESSION['tipas'] == 1){
        $this->load->view("antraste");
        $this->load->view("admin/administratorius");
        $this->load->view("poraste");
                } else { redirect(base_url('auth/prisijungti')); }
        } 
	}
       
        public function admin_kepiniai(){     
        
            if (isset($this->session->userdata['user_logged'])) { 
                    if($_SESSION['tipas'] == 1){            
            $this->load->view("antraste");
            $this->load->view("admin/admin_kepiniai");
            $this->load->view("poraste");                          
            }  else { redirect(base_url('auth/prisijungti')); }
            } 
        }
        
        public function istrintiZinute($zinutes_id){
            if (isset($this->session->userdata['user_logged'])) { 
                            if($_SESSION['tipas'] == 1){
		$result = $this->m->istrintiZinute($zinutes_id);
		if($result){
			$this->session->set_flashdata('success_msg', 'Įrašas ištrintas sėkmingai');
		}else{
			$this->session->set_flashdata('error_msg', 'Nepavyko ištrinti įrašo');
		}
		redirect(base_url('admin/zinutes'));
                            }
            }
	}
        
        public function zinutes(){
            
            if (isset($this->session->userdata['user_logged'])) { 
                            if($_SESSION['tipas'] == 1){
		$data['zinutes'] = $this->m->zinutes();
		$this->load->view('antraste');
		$this->load->view('admin/admin_zinutes', $data);
		$this->load->view('poraste');
                  } else { redirect(base_url('auth/prisijungti')); }
            } 
	}
        
        public function atsiliepimuSarasas(){
            
		$data['atsiliepimai'] = $this->m->atsiliepimai();
		$this->load->view('antraste');
		$this->load->view('atsiliepimai', $data);
		$this->load->view('poraste');
                  
	}
        
        public function istrintiAts($atsiliepimoID){
            if (isset($this->session->userdata['user_logged'])) { 
                            if($_SESSION['tipas'] == 1){
		$result = $this->m->istrintiAts($atsiliepimoID);
		if($result){
			$this->session->set_flashdata('success_msg', 'Įrašas ištrintas sėkmingai');
		}else{
			$this->session->set_flashdata('error_msg', 'Nepavyko ištrinti įrašo');
		}
		redirect(base_url('admin/atsiliepimuSarasas'));
                            }
            }
	}
        
        public function admin_naud(){
            
            if (isset($this->session->userdata['user_logged'])) { 
                            if($_SESSION['tipas'] == 1){
                                
                            
            $data['naud'] = $this->m->getData();
		$this->load->view('antraste');
		$this->load->view('admin/admin_naud', $data);
		$this->load->view('poraste');
                            }
            }
        }
        
        public function edit($naudotojoID){
            if (isset($this->session->userdata['user_logged'])) { 
                            if($_SESSION['tipas'] == 1){
		$data['nau'] = $this->m->getNauById($naudotojoID);
		$this->load->view('antraste');
		$this->load->view('operations/edit', $data);
		$this->load->view('poraste');
                            }
            }
	}
        
        public function update(){
            
            if (isset($this->session->userdata['user_logged'])) { 
                            if($_SESSION['tipas'] == 1){
             
		$result = $this->m->update();
                
            
		if($result){
			$this->session->set_flashdata('success_msg', 'Įrašas atnaujintas sėkmingai');
                        
		}else{
			$this->session->set_flashdata('error_msg', 'Nepavyko atnaujinti įrašo');
                       
                        
		}   redirect(base_url('admin/admin_naud'));
                            } 
            $data['naud'] = $this->m->getData();
            
		$this->load->view('antraste');
		$this->load->view('admin/admin_naud', $data);
		$this->load->view('poraste');
                          
            
            }       
	}
        
        public function delete($naudotojoID){
            if (isset($this->session->userdata['user_logged'])) { 
                            if($_SESSION['tipas'] == 1){
		$result = $this->m->delete($naudotojoID);
		if($result){
			$this->session->set_flashdata('success_msg', 'Įrašas ištrintas sėkmingai');
		}else{
			$this->session->set_flashdata('error_msg', 'Nepavyko ištrinti įrašo');
		}
		redirect(base_url('admin/admin_naud'));
                            }
            }
	}
        
        
        public function admin_uzsakymai(){
            if (isset($this->session->userdata['user_logged'])) { 
                            if($_SESSION['tipas'] == 1){
		$data['uzsakymai'] = $this->m->uzsakymai();
		$this->load->view('antraste');
		$this->load->view('admin/admin_uzsakymai', $data);
		$this->load->view('poraste');
                            }
            }
	}
        
        public function admin_busenos(){
            if (isset($this->session->userdata['user_logged'])) { 
                            if($_SESSION['tipas'] == 1){
		$data['uzsakymai'] = $this->m->patvirtinta();
		$this->load->view('antraste');
		$this->load->view('admin/admin_busenos', $data);
		$this->load->view('poraste');
                            }
            }
	}
        
        
        public function status_update(){
            if (isset($this->session->userdata['user_logged'])) { 
                            if($_SESSION['tipas'] == 1){
               
		$res = $this->m->status_update();
		if($res){
			$this->session->set_flashdata('success_msg', 'Įrašas atnaujintas sėkmingai');
		}else{
			$this->session->set_flashdata('error_msg', 'Nepavyko atnaujinti įrašo');
		}
		redirect(base_url('admin/admin_busenos'));
                            }
            }
	}

        
        public function uzs_istorija()
    {
        if ($_SESSION['user_logged'] == FALSE) {
            $this->session->set_flashdata("error", "Pirmiau užsiregistruokite! ");
            redirect("auth/prisijungti");
        }
            
		$data['uzsakymai'] = $this->m->patvirtinta();
		$this->load->view('antraste');
		$this->load->view('uzs_istorija', $data);
		$this->load->view('poraste');
    }
        
       
}
?>