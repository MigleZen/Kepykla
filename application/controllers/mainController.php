<?php

class mainController extends CI_Controller{
    
    public function index(){
        
        $this->home();       
    }
    
    public function home(){
        $this->load->view("antraste");
        $this->load->view("pagrindinis");
        $this->load->view("poraste");    
    }
    
    public function apiekepyklele(){
        $this->load->view("antraste");
        $this->load->view("apiekepyklele");
        $this->load->view("poraste");
    }
    
    public function kontaktai(){
        if(isset($_POST['siusti'])){
            $this->form_validation->set_rules('kvardas', 'Jūsų vardas', 'required');
            $this->form_validation->set_rules('kpastas', 'El. paštas', 'required');
            $this->form_validation->set_rules('zinute', 'Jūsų žinutė', 'required|min_length[10]|max_length[100]');
            
            if($this->form_validation->run() == TRUE){
                  
            $data = array(
                'kvardas' => $_POST['kvardas'],
                'kpastas' => $_POST['kpastas'],
                'zinute' => $_POST['zinute'],
                'data' => date('Y-m-d')  
                
            );
            $this->db->insert('zinute', $data);
            
            $this->session->set_flashdata("success", "Jūsų žinutė sėkmingai išsiųsta");
            redirect("mainController/kontaktai", "refresh");
        }
        } 
        $this->load->view("antraste");
        $this->load->view("kontaktai");
        $this->load->view("poraste");
    }
    
    
    
     public function atsiliepimai(){
         $this->load->model('admin_model', 'm');
        if(isset($_POST['issiusti'])){
            $this->form_validation->set_rules('avardas', 'Jūsų vardas', 'required');
            $this->form_validation->set_rules('atsiliepimas', 'Atsiliepimas', 'required|min_length[10]|max_length[100]');
            
            if($this->form_validation->run() == TRUE){
                  
            $data = array(
                'avardas' => $_POST['avardas'],
                'atsiliepimas' => $_POST['atsiliepimas'],
                'data' => date('Y-m-d')  
                
            );
            $this->db->insert('atsiliepimai', $data);
            
            $this->session->set_flashdata("success", "Atsiliepimas pridėtas");
            redirect("admin/atsiliepimuSarasas", "refresh");
        }
        } 
        $data['atsiliepimai'] = $this->m->atsiliepimai();
		$this->load->view('antraste');
		$this->load->view('atsiliepimai', $data);
		$this->load->view('poraste');
        
        
            }
            
            
            
             
    
    
}
?>