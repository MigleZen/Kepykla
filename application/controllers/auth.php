<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class auth extends CI_Controller{
    
    public function atsijungti()
    {
        unset($_SESSION);
        session_destroy();
        redirect("auth/prisijungti", "refresh");
    }
   
    public function prisijungti() {
        
            $this->form_validation->set_rules('prisijungimoVardas', 'Prisijungimo vardas', 'required');
            $this->form_validation->set_rules('slaptazodis', 'Slaptažodis', 'required');
            
            if($this->form_validation->run() == TRUE){
                
            $pvardas = $_POST['prisijungimoVardas'];
            $slaptazodis = md5($_POST['slaptazodis']);
            
                
                $this->db->select('*');
                $this->db->from('naudotojas');
                $this->db->where(array('prisijungimoVardas' => $pvardas, 'slaptazodis' => $slaptazodis));
                $query = $this->db->get();
                
                $user = $query->row();
                // jei naudotojas egzistuoja
                if($user->pastas){
                    $this->session->set_flashdata("success", "Sėkmingai prisijungta");
                    
                    $_SESSION['user_logged'] = TRUE;
                    $_SESSION['prisijungimoVardas'] = $user->prisijungimoVardas;
                    $_SESSION['vardas'] = $user->vardas;
                    $_SESSION['pavarde'] = $user->pavarde;
                    $_SESSION['pastas'] = $user->pastas;
                    $_SESSION['numeris'] = $user->numeris;
                    $_SESSION['slaptazodis'] = $user->slaptazodis;
                    $_SESSION['naudotojoID'] = $user->naudotojoID;
                    $_SESSION['tipas'] = $user->tipas;
                    
                    // nukreipt į profilį
                    
                    if($_SESSION['tipas'] == 0){
                    redirect("user/naudotojas", "refresh");
                    } else {
                        redirect("admin", "refresh");
                    }
                    
                } else {
                    $this->session->set_flashdata("error", "Tokio naudotojo nėra");
                    redirect("auth/prisijungti", "refresh");
                } 
                
            }
            
            $this->load->view("antraste");
            $this->load->view("prisijungti");
            $this->load->view("poraste");
        
    }

    public function registracija(){
        if(isset($_POST['registracija'])){
            
            $this->form_validation->set_rules('prisijungimoVardas', 'Prisijungimo vardas', 'required|is_unique[naudotojas.prisijungimoVardas]');
            $this->form_validation->set_rules('vardas', 'Vardas', 'required');
            $this->form_validation->set_rules('pavarde', 'Pavardė', 'required');
            $this->form_validation->set_rules('pastas', 'El. paštas', 'required');
            $this->form_validation->set_rules('numeris', 'Numeris', 'required|numeric|exact_length[9]');
            $this->form_validation->set_rules('slaptazodis', 'Slaptažodis', 'required|min_length[5]');
            $this->form_validation->set_rules('slaptazodis', 'Patvirtinti slaptažodį', 'required|min_length[5]|matches[slaptazodis]');
          
            if($this->form_validation->run() == TRUE){
              //  echo 'Registracija sėkminga';
            // duomenų bazė      
            $data = array(
                'prisijungimoVardas' => $_POST['prisijungimoVardas'],
                'vardas' => $_POST['vardas'],
                'pavarde' => $_POST['pavarde'],
                'pastas' => $_POST['pastas'],
                'numeris' => $_POST['numeris'],
                'slaptazodis' => md5($_POST['slaptazodis']),
                'data' => date('Y-m-d') 
            );
            $this->db->insert('naudotojas', $data);
            
            $this->session->set_flashdata("success", "Jūsų paskyra užregistruota. Galite prisijungti");
            redirect("auth/registracija", "refresh");
        }
        } 
        $this->load->view("antraste");
        $this->load->view("registracija");
        $this->load->view("poraste");
    }
    
}
