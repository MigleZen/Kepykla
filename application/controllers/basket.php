<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Basket extends CI_Controller{

function __construct(){
		parent:: __construct();
		$this->load->model('basket_model', 'bm');
	}	
    
    
public function index(){
    
    $this->load->library('pagination');
    
    $config['base_url'] = base_url().'basket/index';
    //$config['url_segment'] = 3;
    $config['per_page'] = 3;
    
    $page = $this->uri->segment(3,0);
    
    // pagination style
    $config['first_tag_open'] = '<li>';
    $config['first_tag_close'] = '</li>';
    $config['last_tag_open'] = '<li>';
    $config['last_tag_close'] = '</li>';
    $config['prev_tag_open'] = '<li>';
    $config['pre_tag_close'] = '</li>';
    $config['next_tag_open'] = '<li>';
    $config['next_tag_close'] = '</li>';
    $config['num_tag_open'] = '<li>';
    $config['num_tag_close'] = '</li>';
    $config['cur_tag_open'] = '<li class="active"><a href="#">';
    $config['cur_tag_close'] = '</a></li>';
    
    $data['gaminiai'] = $this->bm->findAll($config['per_page'], $page);
    $config['total_rows'] = $this->bm->getTotalRow();
    $this->pagination->initialize($config);
    $data['pagination'] = $this->pagination->create_links();
    
    $this->load->view('antraste');
    $this->load->view('gaminiai', $data);
    $this->load->view('poraste');
    
}

  public function bandeles(){
            
    $data['gaminiai'] = $this->bm->findType();
    $this->load->view('antraste');
    $this->load->view('bandeles', $data);
    $this->load->view('poraste');
    
  }
    
    public function keksiukai(){
    $data['gaminiai'] = $this->bm->findType();
    $this->load->view('antraste');
    $this->load->view('keksiukai', $data);
    $this->load->view('poraste');
    }
    
    public function delete($id){
		$result = $this->bm->delete($id);
		if($result){
			$this->session->set_flashdata('success_msg', 'Įrašas ištrintas sėkmingai');
		}else{
			$this->session->set_flashdata('error_msg', 'Nepavyko ištrinti įrašo');
		}
		redirect(base_url('basket'));
            }
}
?>