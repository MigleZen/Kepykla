<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ShoppingCart extends CI_Controller{

function __construct(){
		parent:: __construct();
		$this->load->model('basket_model', 'bm');
	}	    
    
public function buy($id){
    
if (isset($this->session->userdata['user_logged'])) {
                              
    $product = $this->bm->find($id);
    
    $this->load->library('cart');
  

   $data = array(
        'id'      => $product->id,
        'qty'     => 1,
        'price'   => $product->price,
        'name'    => $product->name,
        'text'    => $product->text,
     
);
$this->cart->product_name_rules = '[:print:]';
$this->cart->insert($data);
         
 

    $this->load->view('antraste');
    $this->load->view('krepselis');
    $this->load->view('poraste');   
    
}
    else {
    redirect("auth/prisijungti", "refresh");
     } 
}

public function delete($rowid){
    $this->cart->update(array('rowid' => $rowid, 'qty' => 0 ));
    $this->load->view('antraste');
    $this->load->view('krepselis');
    $this->load->view('poraste');  
}


public function krepselis(){
    if (isset($this->session->userdata['user_logged'])) {
                              
    $this->load->library('cart');
    $this->cart->product_name_rules = '[:print:]';
    
    $this->load->view('antraste');
    $this->load->view('krepselis');
    $this->load->view('poraste');   
    
}
    else {
    redirect("auth/prisijungti", "refresh");
    }   
}

public function update(){
    $i = 1;
    foreach($this->cart->contents() as $items){
        $this->cart->update(array('rowid' => $items['rowid'], 'qty' => $_POST['qty'.$i]));
        $i++;
    }
    $this->load->view('antraste');
    $this->load->view('krepselis');
    $this->load->view('poraste');
    
}

public function order(){
    
    $db = mysqli_connect("localhost", "root", "", "cinamono_db");
    
    if (isset($this->session->userdata['user_logged'])) { 
    
        $naudotojoID = $_SESSION['naudotojoID'];
        $data = date('Y-m-d');
        $uzsakymoNr = $naudotojoID.'-'.$data;
        $totalPrice = $this->cart->format_number($this->cart->total());
        $totalItems = $this->cart->total_items();
        
        $sql2 = "INSERT INTO patvirtinta (uzsakymoNr, naudotojoID, prekiuKiekis, uzsakymoKaina, data) VALUES ('$uzsakymoNr', '$naudotojoID', '$totalItems', '$totalPrice', '$data')";
        mysqli_query($db, $sql2);
        
        foreach ($this->cart->contents() as $item) {
        $prekes_id = $item['id'];
        $prekes_pav = $item['name'];
        $vnt = $item['qty'];
        $sql = "INSERT INTO uzsakymai (uzsakymoNr, prekes_id, naudotojoID, prekes_pav, vnt, data) VALUES ('$uzsakymoNr', '$prekes_id', '$naudotojoID', '$prekes_pav', '$vnt', '$data')";
        mysqli_query($db, $sql);
                 }
    }
    redirect(base_url('user/naudotojas'));
    
}



}


